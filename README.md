# 1. Overview

This is an implementation of a simple algorithm to predict the most likely price at which a real estate property will be
able to be sold. It's a toy neural net trained on a toy dataset. The code was written as part of an interview for a
position as machine learning engineer. It showcases some skills in designing and implementing machine learning
experiments. However, due to time constraints, we didn't get to implement all the bells and whistles we would do for a
real life project.

## 1.1. Technical Highlights
- the project employs metaparameter search algorithms ([`HyperOpt`](https://hyperopt.github.io/hyperopt/)) to achieve as
high a precision as possible in a relatively short experimentation time, with little human intervention
- the main algorithm that is based on a simple neural net, implemented with [`Keras`](https://keras.io/) using 
[`Theano`](https://github.com/Theano/Theano) as a backend. However, a baseline predictor is also trained,
using algorithms that are quicker to train. More specifically, the search algorithm we used
([`hyperopt-sklearn`](https://github.com/hyperopt/hyperopt-sklearn/tree/master/hpsklearn)) also tries out the various
regressor families provided by the [`scikit-learn`](https://github.com/hyperopt/hyperopt-sklearn/tree/master/hpsklearn)
package and tweaks each of their specific metaparameters.  
- in order to accelerate the experimentation process, we cache the data obtained at various stages in the pipeline
(e.g., preprocessing, trained models etc.)
  
## 1.2. Data Preprocessing
- for `sklearn` regressors, categorical features are converted into one-hot representations
- continuous features are standardized
- given the number of values in each category, using one-hot representations proved to make fitting the data into memory
into a challenge 
- one of the design variables is the UK post code of the property, which has a very high impact on the
predicted value, i.e., the property's price. We managed to find out how to split the post codes into their components,
which, in turn, allowed us to reduce the dimensionality as input features enough to fit the whole dataset into memory
(_distributed representation_)
- the data format for each feature was carefully selected so as not to use more memory than necessary
- features that didn't add any extra information were removed

## 1.3. Neural Net Architecture & Training
- converts each categorical feature from index format to a distributed representation using an `Embedding` Layer
- embedding layers are added together, thus behaving as if each categorical input feature was one-hot
- uses correlation (r2 score) as an optimization metric
- metaparameters searched over include both architecture features (regularization coefficients, number of units in each
layer, dropout probabilities, activation functions), as well as parameters of the optimization process (solver
algorithm, batch size, learning rate, momentum etc.)
- experiments were done with [`hyperas`](https://github.com/maxpumperla/hyperas) 
- cross-validation was used to select the best metaprameters
- early stopping and learning rate annealing policies were used during training

# 2. Setup

2.1. Install a version of python that supports `TensorFlow 1.x`. `Python 3.7` is the latest version that supports that.

2.2. Install dependencies for `Theano`:
- `Theano` has been abandoned a while ago, so installing the right dependencies tends to be a mess. It's probably
easier to try using `TensorFlow` as a backend for `Keras` instead, however, this code was never tested with
`TensorFlow`. [Here](https://web.archive.org/web/20201117011544/https://deeplearning.net/software/theano/install.html)
is the old documentation of how to install everything
- `CUDA`, `nccl`, `python-dev`,
[`libgpuarray & pygpu`](https://web.archive.org/web/20200717151720/http://deeplearning.net/software/libgpuarray/installation.html)

2.3. Create a `Python` virtual environment, activate it and install package dependencies. I've used
[`virtualenv`](https://virtualenv.pypa.io/) from my IDE ([PyCharm](https://www.jetbrains.com/pycharm/)), but you
should also use be able to use the builtin [`venv`](https://docs.python.org/3/library/venv.html) module (you don't
need to install anything to use it, since it's packaged within recent versions of `Python`).
[Here](https://docs.python.org/3/tutorial/venv.html) is a tutorial for `venv`. The commands you need to run should
look something like this if you decide to use `virtualenv`:
- create a virtual environment:
```console
foo@bar:~/housing_price$ virtualenv --python=/usr/bin/python3.7 venv
```
- activate the newly created environment (from `bash`):
```console
foo@bar:~/housing_price$ source venv/bin/activate
```
- install deps:
```console
(venv) foo@bar:~/housing_price$ pip install -r requirements.txt
```

# 3. Running the Code

Alternatives:

3.1. just use `Python` to run the executable scripts within the project:
- [`housing/predict/nnet/__main__.py`](housing/predict/nnet/__main__.py) to run the search algorithm for optimal neural
network metaparams
```console
(venv) foo@bar:~/housing_price$ python -m housing.predict.nnet
```  
- [`housing/predict/util.py`](housing/predict/util.py) to run the search algorithm for `scikit-learn` regressors
```console
(venv) foo@bar:~/housing_price$ python -m housing.predict.util
```

3.2. Use JetBrains' [`PyCharm`](https://www.jetbrains.com/pycharm/) IDE to open the directory (project files reside in
the [`.idea`](.idea) subdirectory) as a project. The project contains run configurations for each of the scripts above,
and a couple more (test scripts).
