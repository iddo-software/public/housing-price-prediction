#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
- model evaluation
- create a performance baseline by automatically exploring sklearn regressors and their hyperparameter spaces
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

import sys
import os
import numpy
from sklearn.model_selection import cross_val_score


class Evaluator(object):
    def __init__(self, X, y, folds=3):
        from sklearn.model_selection import KFold
        self.X = X
        self.y = y
        self.splits = KFold(folds, shuffle=True, random_state=42)

    def __call__(self, model):
        scores = cross_val_score(model, self.X, self.y, cv=self.splits, scoring='r2')
        return numpy.mean(scores)


def best_sklearn_regressor(X, y, trials_filename):
    # try out regressors from sklearn, and get the best one we can in 1.5 h
    old_val = os.environ.get('OMP_NUM_THREADS', None)
    os.environ['OMP_NUM_THREADS'] = '1'
    import hpsklearn

    # @TODO: increase trial timeout, as SVRs need more time to fit the data, and they're probably better than most
    #        regressors

    estim = hpsklearn.HyperoptEstimator(regressor=hpsklearn.any_regressor('reg'),
                                        preprocessing=(),  # we disable preprocessing search, as we wrote it ourselves
                                        # algo=tpe.suggest,
                                        max_evals=200,
                                        trial_timeout=1200,
                                        verbose=True,
                                        fit_increment_dump_filename=trials_filename,
                                        seed=42)

    # on a real problem we would use k-fold xval
    estim.fit(X, y.flatten(), valid_size=0.25, random_state=42)

    model = estim.best_model()['learner']

    # y_test = estim.predict(X_test)

    if old_val is None:
        del os.environ['OMP_NUM_THREADS']
    else:
        os.environ['OMP_NUM_THREADS'] = old_val

    return model


def baseline(X, y, trials_filename=None):
    if trials_filename:
        return best_sklearn_regressor(X, y, trials_filename)

    from sklearn.ensemble import ExtraTreesRegressor
    # one of the models that trained; not a very good one, but it shows what is supposed to happen; 3fold r2: 0.46794699
    return ExtraTreesRegressor(bootstrap=False, criterion='mse', max_depth=None,
                               max_features=0.579254949146, max_leaf_nodes=None,
                               min_impurity_decrease=0.0, min_impurity_split=None,
                               min_samples_leaf=2, min_samples_split=2,
                               min_weight_fraction_leaf=0.0, n_estimators=50, n_jobs=1,
                               oob_score=False, random_state=1, verbose=False, warm_start=False)


def main():
    from sklearn.model_selection import PredefinedSplit
    from housing.dataset.load import load, subset
    data_dir = 'data/'
    # trials_filename = 'trials_sklearn.pkl'  # do model search
    trials_filename = None
    # std_streams_filename = 'trials.txt'  # redirect console output to this file
    std_streams_filename = None
    # use just 10% of the data for training during metaparam search;
    metaparam_search_set_size = .10
    # we use 25% of the data for cross-validation
    xval_size = .25

    X, y, X_test, X_preproc, y_preproc, x_ids_labels, y_ids_labels, y_means, y_stds = load(data_dir)
    # @TODO: as mentioned in the dataset sub-module, we do preprocessor fitting (standardization of continuous features,
    #        which, in our case, is only the time of sale) on the whole data set. In order to better evaluate how well
    #        our model generalizes to unseen samples, we should only fit it on the training set. However, as this does
    #        not influence model selection (actually, it detriments it), we won't do it now. For a fair evaluation,
    #        however, it should be done. We'd probably have to write a custom preprocessor that only standardizes the
    #        features we want.

    X_subset, y_subset = subset(X, y, metaparam_search_set_size)

    if trials_filename:
        trials_filename = os.path.join(data_dir, trials_filename)

    if std_streams_filename:
        std_streams_filename = os.path.join(data_dir, std_streams_filename)
        std_streams_file = open(std_streams_filename, 'wt')
        old_stdout, old_stderr = sys.stdout, sys.stderr
        sys.stdout = sys.stderr = std_streams_file
    try:
        model = baseline(X_subset, y_subset, trials_filename)
    finally:
        if std_streams_filename:
            sys.stdout, sys.stderr = old_stdout, old_stderr
            std_streams_file.close()

    if std_streams_filename:
        print('best sklearn model:')
        print(model)

    # use the same random seed as when evaluating neural nets
    X, y = subset(X, y, size=1., rng=42)  # we don't actually take a subset, but we use this call to shuffle the data

    # @TODO: in a real world situation, we would use a KFold cross-validation strategy with at least 3 folds, but,
    #        because training a net takes so long on my laptop, we'll only do a single fold
    n_xval = int(round(len(y) * xval_size))
    test_fold = numpy.zeros(len(y), int)
    test_fold[:-n_xval] = -1
    cv = PredefinedSplit(test_fold)

    # cv = KFold(...)

    scores = cross_val_score(model, X, y.flatten(), cv=cv, scoring='r2')

    print(scores)


if __name__ == '__main__':
    main()
