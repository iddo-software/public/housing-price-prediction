#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
use hyperopt to search for optimal neural net parameters
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

try:
    import cPickle as pickle
except ImportError:
    import pickle


def get_search_space(n_label_features):
    from six.moves import xrange
    from hyperopt.hp import quniform, choice
    space = {
        # @TODO: add selu after updating keras
        'model_kwargs': {
            'embedding_l1s': [2 ** quniform('embedding_l1s_var' + str(idx), -46, 0, 1)
                              for idx in xrange(n_label_features)],
            'embedding_l2s': [2 ** quniform('embedding_l2s_var' + str(idx), -46, 0, 1)
                              for idx in xrange(n_label_features)],
            'hid_act': choice('layer1_act_var', ['relu', 'tanh', 'sigmoid']),
            'layer1_n_units': 100 * (1 + 2 ** quniform('layer1_n_units_var', 0, 4, 1)),

            'layer1_l1': 2 ** quniform('layer1_l1_var', -46, 0, 1),
            'layer1_l2': 2 ** quniform('layer1_l2_var', -46, 0, 1),
            'layer1_dropout': quniform('layer1_dropout_var', 0, 0.7, 0.1),

            'layer2_n_units': 100 * (1 + 2 ** quniform('layer2_n_units_var', 0, 4, 1)),
            'layer2_l1': 2 ** quniform('layer2_l1_var', -46, 0, 1),
            'layer2_l2': 2 ** quniform('layer2_l2_var', -46, 0, 1),
            'layer2_dropout': quniform('layer2_dropout_var', 0, 0.7, 0.1),

            'layer3_n_units': 100 * (1 + 2 ** quniform('layer3_n_units_var', 0, 4, 1)),
            'layer3_dropout': quniform('layer3_dropout_var', 0, 0.7, 0.1),
            'layer3_l1': 2 ** quniform('layer3_l1_var', -46, 0, 1),
            'layer3_l2': 2 ** quniform('layer3_l2_var', -46, 0, 1),

            'out_l1': 2 ** quniform('out_l1_var', -46, 0, 1),
            'out_l2': 2 ** quniform('out_l2_var', -46, 0, 1)
        },
        'train_kwargs': choice('solver_type', [{'solver': 'nadam',
                                                'solver_kwargs': {'lr': 2 ** quniform('nadam_lr', -46, 3, 1)},
                                                'batchsize': 2 ** quniform('nadam_batchsize', 6, 9, 1)},
                                               {'solver': 'sgd',
                                                'solver_kwargs': {
                                                    'lr': 2 ** quniform('sgd_lr', -46, 3, 1),
                                                    'momentum': quniform('sgd_momentum', 0, .9, .1),
                                                    'nesterov': True},
                                                'batchsize': 2 ** quniform('sgd_batchsize', 5, 9, 1)
                                                }]),
    }
    return space


def safe_save(obj, filename):
    import shutil
    with open(filename + '.tmp', 'wb') as temp_file:
        pickle.dump(obj, temp_file)

    shutil.move(filename + '.tmp', filename)


def best_net(inputs_train, outputs_train, inputs_xval, outputs_xval, n_labels_per_label_feature, max_epochs, max_evals,
             trials_filename, force=False):
    import os
    import gc
    import traceback

    import numpy
    from hyperopt import STATUS_OK, STATUS_FAIL, fmin, tpe, Trials

    n_dense_features = inputs_train[0].shape[1]

    if max_evals == 0:
        model_kwargs = {'n_dense_features': n_dense_features,
                        'n_labels_per_label_feature': n_labels_per_label_feature}
        train_kwargs = {}
        return model_kwargs, train_kwargs
        # this model crashes during search... don't know why, though, as, if specified here, it works
        # best_args = {
        #     'model_kwargs': {'embedding_l1s': (2.9802322387695312e-08, 1.52587890625e-05),
        #                      'embedding_l2s': (1.9073486328125e-06, 2.9103830456733704e-11),
        #                      'layer1_act': 'relu',
        #                      'layer1_dropout': 0.2,
        #                      'layer1_l1': 1.0,
        #                      'layer1_l2': 1.862645149230957e-09,
        #                      'layer1_n_units': 500,
        #                      'layer2_act': 'tanh',
        #                      'layer2_dropout': 0.1,
        #                      'layer2_l1': 3.725290298461914e-09,
        #                      'layer2_l2': 6.103515625e-05,
        #                      'layer2_n_units': 1700,
        #                      'layer3_act': 'tanh',
        #                      'layer3_dropout': 0.2,
        #                      'layer3_l1': 7.62939453125e-06,
        #                      'layer3_l2': 0.0001220703125,
        #                      'layer3_n_units': 200,
        #                      'out_l1': 2.384185791015625e-07,
        #                      'out_l2': 1.8189894035458565e-12},
        #     'train_kwargs': {'batchsize': 64,
        #                      'solver': 'nadam',
        #                      'solver_kwargs': {'lr': 2.842170943040401e-14}
        #                      }
        # }

    if os.path.isfile(trials_filename) and not force:
        with open(trials_filename, 'rb') as trials_file:
            trials = pickle.load(trials_file)
    else:
        space = get_search_space(len(n_labels_per_label_feature))

        net_filename = None

        trials = Trials()

        def objective(args):
            from housing.predict.nnet import config
            config.keras()
            from housing.predict.nnet.models import train_nnet

            safe_save(trials, trials_filename)

            net_kwargs = args['model_kwargs']
            net_kwargs['n_dense_features'] = n_dense_features
            net_kwargs['n_labels_per_label_feature'] = n_labels_per_label_feature
            net_kwargs['layer1_n_units'] = int(round(net_kwargs['layer1_n_units']))
            net_kwargs['layer2_n_units'] = int(round(net_kwargs['layer2_n_units']))
            net_kwargs['layer3_n_units'] = int(round(net_kwargs['layer3_n_units']))

            train_kwargs = args['train_kwargs']
            train_kwargs['batchsize'] = int(round(train_kwargs['batchsize']))

            try:
                best_corr_xval = train_nnet(net_kwargs, inputs_train, outputs_train, inputs_xval, outputs_xval,
                                            net_filename, max_epochs, **train_kwargs)
            except:
                print('model training failed!')
                traceback.print_exc()
                return {'status': STATUS_FAIL}
            else:
                objective.max = max(best_corr_xval, objective.max)
                print('experiment:', objective.iter, '/', max_evals, '; best corr:', objective.max)
                # return {'loss': train_loss_at_best_xval, 'true_loss': -best_corr_xval, 'status': STATUS_OK,
                #                 'model': model}
                return {'loss': -best_corr_xval, 'status': STATUS_OK, 'model_kwargs': net_kwargs,
                        'train_kwargs': train_kwargs}
            finally:
                objective.iter += 1
                gc.collect()  # try to help free some memory...

        objective.max = -numpy.inf
        objective.iter = 1

        fmin(objective, space, tpe.suggest, max_evals, trials=trials, rstate=numpy.random.RandomState(42))

        safe_save(trials, trials_filename)

    ok_id = []
    for status_id, status in enumerate(trials.statuses()):
        if status == STATUS_OK:
            ok_id.append(status_id)

    losses = trials.losses()
    losses = [numpy.inf if loss is None else loss for loss in losses]
    min_loss = min(losses)
    min_loss_id_losses = losses.index(min_loss)
    min_loss_id = ok_id[min_loss_id_losses]
    best_args = trials.results[min_loss_id]
    model_kwargs = best_args['model_kwargs']
    train_kwargs = best_args['train_kwargs']

    # @TODO: remove after rerunning the search
    model_kwargs['n_labels_per_label_feature'] = n_labels_per_label_feature

    return model_kwargs, train_kwargs


def main():
    import hyperopt.pyll.stochastic
    space = get_search_space(n_label_features=2)
    # from hyperopt import hp
    # space = hp.choice('a',
    #                   [
    #                       ('case 1', 1 + hp.lognormal('c1', 0, 1)),
    #                       ('case 2', hp.uniform('c2', -10, 10))
    #                   ])
    # space = [
    #     hp.choice('a', ['a1', 'a2']),
    #     hp.choice('b', ['b1', 'b2'])
    # ]
    print(hyperopt.pyll.stochastic.sample(space))


if __name__ == '__main__':
    main()
