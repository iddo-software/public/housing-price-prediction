from six.moves import xrange


def iterable_or_none(var):
    """

    :param var:
    :return: None if bool(var) evaluates to False but var is not 0, else var
    """
    try:
        is_true = bool(var)
    except ValueError:
        return var
    else:
        if is_true or var == 0:
            return [var]

        return None


def get_n_labels_per_label_feature(X, x_ids_labels):
    x_ids_labels = iterable_or_none(x_ids_labels)
    if x_ids_labels is None:
        return 0

    return X[:, x_ids_labels].max(axis=0).astype(int) + 1


def prep_for_net(X, y, x_ids_labels, y_ids_labels):
    x_ids_labels = iterable_or_none(x_ids_labels)
    y_ids_labels = iterable_or_none(y_ids_labels)

    if x_ids_labels is not None:
        X_labels = X[:, x_ids_labels]
        X = X[:, [col_id for col_id in xrange(X.shape[1]) if col_id not in x_ids_labels]]

    if y_ids_labels is not None:
        y_labels = y[:, y_ids_labels]
        y = y[:, [col_id for col_id in xrange(y.shape[1]) if col_id not in y_ids_labels]]

    inputs = [X]

    if x_ids_labels is not None:
        inputs.extend([X_labels[:, col_id] for col_id in xrange(X_labels.shape[1])])

    if y is not None:
        outputs = [y]
        if y_ids_labels is not None:
            outputs.extend([y_labels[:, col_id] for col_id in xrange(y_labels.shape[1])])
    else:
        outputs = None

    return inputs, outputs
