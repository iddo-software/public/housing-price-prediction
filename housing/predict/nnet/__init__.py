#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
this subpackage implements a neural net architecture, or, actually, a family of neural nets induced by some variable
hyperparameters. It also has code for training nets, and automatically searching for the optimal hyperparameters,
both wrt model architecture, and wrt model optimization
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"
