#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
use hyperas to search for optimal neural net parameters
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"


import math

from hyperopt import Trials, STATUS_OK, tpe

# from housing.predict.nnet import config
# config.keras(machine='mielu')
#
# from keras.models import Model
# from keras.layers import Input, Dense, Activation, Embedding, Add, Flatten, Dropout
# from keras.regularizers import l1_l2
# from keras.optimizers import Nadam, SGD
# from keras.losses import mean_squared_error

from housing.predict.nnet.util import get_n_labels_per_label_feature, prep_for_net
from housing.predict.nnet.models import simple_net  # , corr


# def make_net(n_dense_features, n_labels_per_label_feature):
#     input_dense = Input(shape=(n_dense_features,))
#     layer1_dense = Dense(layer1_n_units, kernel_regularizer=l1_l2(layer1_l1, layer1_l2))(input_dense)
#
#     label_inputs = []
#     emb_layers = []
#
#     for n_labels, emb_l1, emb_l2 in zip(n_labels_per_label_feature, embedding_l1s, embedding_l2s):
#         label_input = Input(shape=(1,))
#         label_inputs.append(label_input)
#         emb_layer = Embedding(input_dim=n_labels, output_dim=layer1_n_units, input_length=1,
#                               embeddings_regularizer=l1_l2(emb_l1, emb_l2))(label_input)
#         flat_emb_layer = Flatten()(emb_layer)
#         emb_layers.append(flat_emb_layer)
#
#     layer1 = Add()([layer1_dense] + emb_layers)
#     layer1 = Activation(layer1_act)(layer1)
#     layer1 = Dropout(layer1_dropout)(layer1)
#
#     layer2 = Dense(layer2_n_units, activation=layer2_act, kernel_regularizer=l1_l2(layer2_l1, layer2_l2))(layer1)
#     layer2 = Dropout(layer2_dropout)(layer2)
#
#     layer3 = Dense(layer3_n_units, activation=layer3_act, kernel_regularizer=l1_l2(layer3_l1, layer3_l2))(layer2)
#     layer3 = Dropout(layer3_dropout)(layer3)
#     pred = Dense(1, activation='relu', kernel_regularizer=l1_l2(out_l1, out_l2))(layer3)
#
#     model = Model(inputs=[input_dense] + label_inputs, outputs=pred)
#
#     # although, technically, this is not part of the model, but we leave it here for metaparam search
#     # @TODO: parametrize
#     model.compile(optimizer=Nadam(lr=4e-4, beta_1=.9, beta_2=.999, epsilon=1e-8, schedule_decay=4e-3),
#                   loss=mean_squared_error, metrics=[corr])
#
#     return model


def sample_params(n_labels_per_label_feature):
    from housing.predict.nnet import config
    config.keras()
    from hyperas.distributions import quniform, loguniform, choice, conditional, qloguniform

    # in order to get values the log of which lies between 0 and 1, we must sample between -inf and 0
    embedding_l1s = {{loguniform(math.log(1e-14), 0, size=(len(n_labels_per_label_feature),))}}
    embedding_l2s = {{loguniform(math.log(1e-14), 0, size=(len(n_labels_per_label_feature),))}}
    layer1_n_units = {{quniform(100, 1000, 32)}}
    layer1_n_units = int(round(layer1_n_units))
    layer1_act = {{choice('relu', 'selu', 'tanh', 'sigmoid')}}
    layer1_l1 = {{loguniform(math.log(1e-14), 0)}}
    layer1_l2 = {{loguniform(math.log(1e-14), 0)}}
    layer1_dropout = {{quniform(0, 0.7, 0.1)}}

    layer2_n_units = {{quniform(100, 2000, 32)}}
    layer2_n_units = int(round(layer2_n_units))
    layer2_act = {{choice('relu', 'selu', 'tanh', 'sigmoid')}}
    layer2_dropout = {{quniform(0, 0.7, 0.1)}}
    layer2_l1 = {{loguniform(math.log(1e-14), 0)}}
    layer2_l2 = {{loguniform(math.log(1e-14), 0)}}

    layer3_n_units = {{quniform(100, 2000, 32)}}
    layer3_n_units = int(round(layer3_n_units))
    layer3_act = {{choice('relu', 'selu', 'tanh', 'sigmoid')}}
    layer3_dropout = {{quniform(0, 0.7, 0.1)}}
    layer3_l1 = {{loguniform(math.log(1e-14), 0)}}
    layer3_l2 = {{loguniform(math.log(1e-14), 0)}}

    out_l1 = {{loguniform(math.log(1e-14), 0)}}
    out_l2 = {{loguniform(math.log(1e-14), 0)}}

    solver = conditional({{choice('nadam', 'sgd')}})

    # we'd like to try out learning rates between 1e-14 and 10, but we'd only like to sample powers of two, so what we
    # need is 2 ** -46, 2 ** -45, ..., 2 ** 3
    # the qloguniform only allows natural logarithms, so we're going to sample
    # e ** -46, e ** -45, ..., e ** 3
    # and change base
    # change of base formula : log(x, 2) = log(x) / log(2)
    #                          log(x)    = log(x, 2) / log(math.e, 2)
    lr = {{qloguniform(-46, 3, 1)}}
    lr /= math.log(2)

    solver_kwargs = {'lr': lr}

    if solver == 'sgd':
        solver_kwargs['momentum'] = {{quniform(0, .9, .1)}}
        solver_kwargs['nesterov'] = True

    # we want to try out batchsizes 32, 64, 128, ..., 1024, which is equivalent to
    # 2 ** 5, 2 ** 6, ..., 2 ** 10
    # the qloguniform only allows natural logarithms, so we're going to sample
    # e ** 5, e ** 6, ..., e ** 10
    # and change base
    # change of base formula : log(x, 2) = log(x) / log(2)
    #                          log(x)    = log(x, 2) / log(math.e, 2)

    batchsize = {{qloguniform(5, 10, 1)}}
    batchsize /= math.log(2)
    batchsize = int(round(batchsize))

    net_params = (embedding_l1s, embedding_l2s,
                  layer1_n_units, layer1_act, layer1_l1, layer1_l2, layer1_dropout,
                  layer2_n_units, layer2_act, layer2_l1, layer2_l2, layer2_dropout,
                  layer3_n_units, layer3_act, layer3_l1, layer3_l2, layer3_dropout,
                  out_l1, out_l2)

    train_params = (solver, solver_kwargs, batchsize)

    return net_params, train_params


def try_net(inputs, outputs, n_dense_features, n_labels_per_label_feature,
            validation_split, net_filename, max_epochs):
    from housing.predict.nnet import config
    config.keras()
    from keras.models import load_model
    from housing.predict.nnet.models import train_nnet, NegCorr
    net_params, train_params = sample_params(n_labels_per_label_feature)
    net = simple_net(n_dense_features, n_labels_per_label_feature, *net_params)

    train_nnet(net, inputs, outputs, validation_split, net_filename, max_epochs, *train_params)

    best_corr_xval = max(net.history.history['val_corr'])

    # get best model
    net = load_model(net_filename, custom_objects={'neg_corr': NegCorr()})

    return {'loss': -best_corr_xval, 'status': STATUS_OK, 'model': net}


def best_net(X, y, x_ids_labels, y_ids_labels, subset_size,
             validation_split, net_filename, max_epochs,
             max_evals):
    from housing.dataset.load import subset

    n_labels_per_label_feature = get_n_labels_per_label_feature(X, x_ids_labels)
    X, y = subset(X, y, size=subset_size)  # no shuffle (already shuffled)
    inputs, outputs, n_dense_features = prep_for_net(X, y, x_ids_labels, y_ids_labels)

    if max_evals == 0:
        best_model = simple_net(n_dense_features, n_labels_per_label_feature)

        # @TODO: extract default arguments of train_nnet
        best_solver = 'nadam'
        best_solver_kwargs = {'lr': 4e-4}
        best_batchsize = 256
        best_train_params = (best_solver, best_solver_kwargs, best_batchsize)
        return best_model, best_train_params

    from housing.predict.nnet import config
    config.keras()
    from hyperas import optim

    def data():
        return inputs, outputs, n_dense_features, n_labels_per_label_feature, validation_split, net_filename, max_epochs

    best_run, best_model = optim.minimize(model=try_net,
                                          data=data,
                                          algo=tpe.suggest,
                                          max_evals=max_evals,
                                          trials=Trials(),
                                          verbose=True,
                                          rseed=42)

    print('hyperparams of best run:')
    print(best_run)

    # @TODO: extract params from best_run
    best_solver = 'nadam'
    best_solver_kwargs = {'lr': 4e-4}
    best_batchsize = 256
    best_train_params = (best_solver, best_solver_kwargs, best_batchsize)

    return best_model, best_train_params


