#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
build and train a simple neural net
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

from housing.predict.nnet import config


def true_corr(model, inputs, outputs, train_kwargs):
    import numpy
    import math

    kwargs = {'batch_size': train_kwargs['batchsize']} if 'batchsize' in train_kwargs else {}
    pred = model.predict(inputs, verbose=1, **kwargs).squeeze()

    pred -= pred.mean()
    pred /= math.sqrt(pred.dot(pred) / len(pred))

    true = numpy.asarray(outputs).squeeze()
    true -= true.mean()
    true /= math.sqrt(true.dot(true) / len(true))

    return pred.dot(true) / len(pred)


class NegCorr(object):
    def __init__(self, pred_mean=None, pred_std=None, true_mean=None, true_std=None):
        self.pred_mean = pred_mean
        self.pred_std = pred_std
        self.true_mean = true_mean
        self.true_std = true_std
        self.__name__ = 'neg_corr'

    def __call__(self, y_pred, y_true):
        import numpy
        config.keras()
        from keras import backend as K

        # version 1: nans
        # y = (y - K.mean(y)) / K.std(y)
        # y_true = (y_true - K.mean(y_true)) / K.std(y_true)
        # return K.mean(y * y_true)

        # version 2: nans
        # mean_y = K.mean(y)
        # mean_y_true = K.mean(y_true)
        #
        # s_y = K.sqrt(K.mean(y * y) - mean_y * mean_y)
        # s_y_true = K.sqrt(K.mean(y_true * y_true) - mean_y_true * mean_y_true)
        #
        # cov = K.mean(y * y_true) - mean_y * mean_y_true
        #
        # return cov / (s_y * s_y_true)

        # version 3: more numerically stable. checks for zero stds

        dtype = K.dtype(y_pred)
        eps = numpy.finfo(dtype).eps
        zero = K.zeros((1,), dtype)

        if self.pred_std is not None and self.pred_std <= eps or self.true_std is not None and self.true_std <= eps:
            # both stds known, at least one of them 0
            return zero

        y_pred -= K.mean(y_pred) if self.pred_mean is None else self.pred_mean
        pred_std = K.sqrt(K.mean(y_pred * y_pred)) if self.pred_std is None else self.pred_std
        y_pred /= pred_std

        y_true -= K.mean(y_true) if self.true_mean is None else self.true_mean
        true_std = K.sqrt(K.mean(y_true * y_true)) if self.true_std is None else self.true_std
        y_true /= true_std

        corr = K.mean(y_pred * y_true)

        # either keras or its backends (theano and tensorflow) should be able to optimize constant expressions out, so
        # the following stuff is probably useless. However, we'd like to be sure, so we write it ourselves

        if self.pred_std is not None and self.true_std is not None:
            # both stds known, none of them 0
            return -corr

        if self.pred_std is None and self.true_std is None:
            # neither std is known
            stds_positive = K.greater(pred_std, eps) & K.greater(true_std, eps)
        elif self.pred_std is None:
            # true std is known and is not 0
            stds_positive = K.greater(pred_std, eps)
        elif self.true_std is None:
            # pred std is known and is not 0
            stds_positive = K.greater(true_std, eps)
        else:
            raise Exception('How has it come to this...?')  # get rid of warnings

        return K.switch(stds_positive, -corr, zero)


def train_nnet(model_kwargs, inputs_train, outputs_train, inputs_xval, outputs_xval,
               net_filename, max_epochs, solver='nadam', solver_kwargs={'lr': 4e-4}, batchsize=256,
               metric='neg_corr'):
    config.keras()
    from keras.optimizers import Nadam, SGD
    from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
    from keras.losses import mean_squared_error
    from keras.models import load_model

    if solver == 'nadam':
        optimizer = Nadam(**solver_kwargs)
    elif solver == 'sgd':
        optimizer = SGD(**solver_kwargs)
    else:
        raise ValueError('unknown optimizer type:' + str(solver))

    metrics = None
    if True:
        if metric == 'neg_corr':
            # average of batch correlation is a good enough approximation for the overall
            neg_corr = NegCorr()
            metrics = [neg_corr]
            assert metric == neg_corr.__name__
        elif metric != 'loss':
            metrics = [metric]
    else:
        # older idea, not so great...
        # we don't care about the correlation on the training batches, so we just initialize the correlation object with
        # the statistics of the cross-validation dataset's outputs
        n_samples = len(inputs[0])
        train_samples = int(n_samples * (1. - validation_split))  # split the same as in keras.model.fit
        xval_out = outputs[0][train_samples:]
        xval_out_mean = xval_out.mean()
        xval_out_std = xval_out.std()

        # we assume our model is powerful enough to learn the dataset's mean and std, so the empirical statistics should
        # serve as good estimators for the statistics of the net's output
        neg_corr = NegCorr(pred_mean=xval_out_mean, pred_std=xval_out_std, true_mean=xval_out_mean,
                           true_std=xval_out_std)

    model = simple_net(**model_kwargs)

    model.compile(optimizer=optimizer, loss=mean_squared_error, metrics=metrics)

    epsilon = .005

    monitor = 'val_' + metric

    # @TODO: for a real life scenario: create our own EarlyStopping implementation that supports decisions based on
    #        relative improvement, and that increases patience proportionally with the number of epochs so far; also,
    #        a minimum number of epochs should be allowed as param
    early_stoppping = EarlyStopping(monitor, min_delta=epsilon, patience=3, verbose=1)

    lr_schedule = ReduceLROnPlateau(monitor, factor=0.25, patience=1, verbose=1, epsilon=epsilon, cooldown=0,
                                    min_lr=1e-12)

    callbacks = [early_stoppping, lr_schedule]

    if net_filename:
        model_checkpoint = ModelCheckpoint(net_filename, monitor, verbose=1, save_best_only=True)
        callbacks.append(model_checkpoint)

    # @TODO: implementation for early stop, lr_schedule callbacks  and model_checkpoint that do their thing every n
    #        batches instead of only at the end of an epoch; also, properly evaluate correlation (i.e., not average over
    #        batch correlations)

    print('training net with solver', solver, solver_kwargs, 'batchsize:', batchsize)

    model.fit(inputs_train, outputs_train, batch_size=batchsize, epochs=max_epochs, callbacks=callbacks,
              validation_data=(inputs_xval, outputs_xval), shuffle=False)

    xval_metrics = model.history.history[monitor]

    # use same logic as when 'auto' mode (default) is used for early stopping, lr reduce, model checkpoint
    if 'acc' in metric:
        best_xval = -max(xval_metrics)
    else:
        best_xval = min(xval_metrics)

    if not net_filename:
        return best_xval

    epoch_best_xval = xval_metrics.index(best_xval)

    model = load_model(net_filename, custom_objects={'neg_corr': neg_corr} if metric == 'neg_corr' else None)

    print('evaluating loss on whole training set...')
    train_metrics_at_best_xval = model.evaluate(inputs_train, outputs_train, batchsize)

    train_metrics_at_best_xval = OrderedDict(zip(model.metrics_names, train_metrics_at_best_xval))

    train_loss_at_best_xval = train_metrics_at_best_xval['loss']

    return model, train_loss_at_best_xval, epoch_best_xval


def retrain_net(model, inputs, outputs, train_loss_at_best_xval, epoch_best_xval, batchsize=256):
    config.keras()
    from keras.callbacks import Callback

    class StopAtValue(Callback):
        def __init__(self, monitor, batch_eval_freq, stop_at, mode='max'):
            assert mode in ('min', 'max')
            super(StopAtValue, self).__init__()
            self.monitor = monitor
            self.stop_at = stop_at
            self.mode = mode
            self.eval_freq = batch_eval_freq
            
        def on_batch_end(self, batch, logs=None):
            import warnings
            from collections import OrderedDict
            if batch % self.eval_freq == 0:
                # the training loss in the logs dict argument is an average over batch losses, which is not very
                # precise, as the model changes at every iteration; that's why we prefer to compute the error on the
                # whole training set here
                logs = self.model.evaluate(inputs, outputs, batchsize, verbose=0)
                logs = OrderedDict(zip(self.model.metrics_names, logs))
                current = logs.get(self.monitor, None)
                if current is None:
                    warnings.warn('Stop at error requires %s available!' % self.monitor, RuntimeWarning)
                    self.model.stop_training = True
                else:
                    if self.mode == 'max' and current >= self.stop_at or self.mode == 'min' and current <= self.stop_at:
                        self.model.stop_training = True

    stop_at_error = StopAtValue('loss', batch_eval_freq=25, stop_at=train_loss_at_best_xval, mode='min')

    prev_epochs = epoch_best_xval + 1
    epochs = int(round(prev_epochs + prev_epochs / 2.))

    model.fit(inputs, outputs, batch_size=batchsize, epochs=epochs, callbacks=[stop_at_error], initial_epoch=prev_epochs)

    return model


def simple_net(n_dense_features, n_labels_per_label_feature,
               embedding_l1s=(0, 0), embedding_l2s=(1e-14, 1e-14),
               hid_act='relu',
               layer1_n_units=200, layer1_l1=0, layer1_l2=2e-5, layer1_dropout=0,
               layer2_n_units=400, layer2_l1=0, layer2_l2=1e-4, layer2_dropout=0,
               layer3_n_units=150, layer3_l1=0, layer3_l2=2e-5, layer3_dropout=0,
               out_l1=0, out_l2=1e-6):
    """
    - turn each categorical input feature into a dense embedding. Embeddings are summed up to form
    a single vector representing all categorical variables in a dense format
    - continuous features are fed into a fully connected layer the output of which is, then, added to the embedding
    (fully connected layer has the same number of units as the embedding layers)
    - two other hidden layers
    - one output layer
    """
    import inspect
    config.keras()
    from keras.models import Model
    from keras.layers import Input, Dense, Activation, Embedding, Add, Flatten, Dropout
    from keras.regularizers import l1_l2
    from keras import backend as K
    from keras.initializers import glorot_uniform, VarianceScaling, he_normal

    embedding_l1s = K.cast_to_floatx(embedding_l1s)
    embedding_l2s = K.cast_to_floatx(embedding_l2s)

    layer1_l1 = K.cast_to_floatx(layer1_l1)
    layer1_l2 = K.cast_to_floatx(layer1_l2)
    layer1_dropout = K.cast_to_floatx(layer1_dropout)

    layer2_l1 = K.cast_to_floatx(layer2_l1)
    layer2_l2 = K.cast_to_floatx(layer2_l2)
    layer2_dropout = K.cast_to_floatx(layer2_dropout)

    layer3_l1 = K.cast_to_floatx(layer3_l1)
    layer3_l2 = K.cast_to_floatx(layer3_l2)
    layer3_dropout = K.cast_to_floatx(layer3_dropout)

    out_l1 = K.cast_to_floatx(out_l1)
    out_l2 = K.cast_to_floatx(out_l2)

    # formerly, layers were allowed different activations, but the models' performance used to vary significantly,
    # indicating that the random weight initialization has a powerful impact, thus, deducing that there's the
    # initialization distributions were unsuited for the activation types

    seed = 42

    if hid_act == 'tanh':
        weight_init = glorot_uniform
    elif hid_act == 'sigmoid':
        # half the variance in both inputs and outputs for
        def weight_init(seed=None):
            return VarianceScaling(scale=4, mode='fan_avg', distribution='uniform', seed=seed)
    elif hid_act == 'relu':
        weight_init = he_normal
    else:
        raise TypeError('unknown activation ' + str(hid_act))

    print('constructing net:')
    print(inspect.formatargvalues(*inspect.getargvalues(inspect.currentframe())))

    input_dense = Input(shape=(n_dense_features,))
    layer1_dense = Dense(layer1_n_units, kernel_initializer=weight_init(seed),
                         kernel_regularizer=l1_l2(layer1_l1, layer1_l2))(input_dense)

    seed += 1

    label_inputs = []
    emb_layers = []

    for n_labels, emb_l1, emb_l2 in zip(n_labels_per_label_feature, embedding_l1s, embedding_l2s):
        label_input = Input(shape=(1,))
        label_inputs.append(label_input)
        emb_layer = Embedding(input_dim=n_labels, output_dim=layer1_n_units,
                              embeddings_initializer=weight_init(seed),
                              embeddings_regularizer=l1_l2(emb_l1, emb_l2),
                              input_length=1)(label_input)
        flat_emb_layer = Flatten()(emb_layer)
        emb_layers.append(flat_emb_layer)
        seed += 1

    layer1 = Add()([layer1_dense] + emb_layers)
    layer1 = Activation(hid_act)(layer1)

    layer1 = Dropout(layer1_dropout, seed=seed)(layer1)
    seed += 1

    layer2 = Dense(layer2_n_units, activation=hid_act,
                   kernel_initializer=weight_init(seed),
                   kernel_regularizer=l1_l2(layer2_l1, layer2_l2))(layer1)
    seed += 1

    layer2 = Dropout(layer2_dropout, seed=seed)(layer2)
    seed += 1

    layer3 = Dense(layer3_n_units, activation=hid_act,
                   kernel_initializer=weight_init(seed),
                   kernel_regularizer=l1_l2(layer3_l1, layer3_l2))(layer2)

    layer3 = Dropout(layer3_dropout, seed=seed)(layer3)
    seed += 1

    pred = Dense(1, activation='linear',
                 kernel_initializer=he_normal(seed),
                 kernel_regularizer=l1_l2(out_l1, out_l2))(layer3)

    model = Model(inputs=[input_dense] + label_inputs, outputs=pred)

    return model
