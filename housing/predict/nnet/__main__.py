#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
build and train a simple neural net
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"


def main():
    import numpy
    import os
    from socket import gethostname

    from housing.dataset.load import load, subset
    from housing.predict.nnet.util import prep_for_net, get_n_labels_per_label_feature
    from housing.predict.nnet.models import train_nnet, retrain_net, true_corr, NegCorr
    from housing.predict.nnet.search import best_net
    from housing.predict.nnet import config

    machine = gethostname()

    # user params
    config.keras('theano', machine)
    from keras.models import load_model

    if machine == 'mielu':
        data_dir = 'data'
    elif machine == 'nicu':
        data_dir = 'data'
    elif machine == 'mirel':
        data_dir = 'data'
    else:
        raise NotImplementedError('unkown machine: ' + str(machine))

    xval_split = .25
    net_filename = 'final_net.pkl'
    trials_filename = 'net_trials.pkl'

    debug = False
    skip_search = False

    force_search = True
    force_train = False
    do_predict = False

    if debug:
        # 12%, from which only 10% are used for searching metaparam, so about 1100 samples (512 is largest batch size)
        subset_size = .25
        max_epochs = 5  # final net training epochs
        max_epochs_search = 2
    else:
        subset_size = 1
        max_epochs = 30  # final net training epochs
        max_epochs_search = 20

    if skip_search:
        max_evals = 0
    else:
        if debug:
            max_evals = 10
        else:
            max_evals = 1000

    # use only 10% of the data while searching for the best metaparameters
    search_subset_size = .25

    # @TODO: switch to loss!!!
    metric = 'neg_corr'  # can also be 'corr'
    # ~user params

    if force_search:
        force_train = True

    net_filename = os.path.join(data_dir, net_filename)
    trials_filename = os.path.join(data_dir, trials_filename)

    X, y, X_test, X_preproc, y_preproc, x_ids_labels, y_ids_labels, y_mean, y_std = load(data_dir)

    # X_test contains labels that X doesn't
    X_all = numpy.vstack((X, X_test))

    n_labels_per_label_feature = get_n_labels_per_label_feature(X_all, x_ids_labels)

    # shuffle the same way we did on the sklearn models
    X, y = subset(X, y, size=1, rng=42)

    n_train = len(X)

    n_train = int(round(n_train * (1 - xval_split)))

    X_train = X[:n_train]
    y_train = y[:n_train]

    X_xval = X[n_train:]
    y_xval = y[n_train:]

    n_train = int(round(n_train * subset_size))
    X_train = X_train[:n_train]
    y_train = y_train[:n_train]

    n_train = int(round(n_train * search_subset_size))

    X_train_search = X_train[:n_train]
    y_train_search = y_train[:n_train]

    inputs_train_search, outputs_train_search = prep_for_net(X_train_search, y_train_search, x_ids_labels, y_ids_labels)
    inputs_xval, outputs_xval = prep_for_net(X_xval, y_xval, x_ids_labels, y_ids_labels)

    model_kwargs, train_kwargs = best_net(inputs_train_search, outputs_train_search, inputs_xval, outputs_xval,
                                          n_labels_per_label_feature, max_epochs_search, max_evals, trials_filename,
                                          force_search)

    if os.path.isfile(net_filename) and not force_train:
        model = load_model(net_filename, custom_objects={'neg_corr': NegCorr()} if metric == 'neg_corr' else None)
    else:
        inputs_train, outputs_train = prep_for_net(X_train, y_train, x_ids_labels, y_ids_labels)

        model, train_loss_at_best_xval, epoch_best_xval \
            = train_nnet(model_kwargs, inputs_train, outputs_train, inputs_xval, outputs_xval,
                         net_filename, max_epochs, **train_kwargs)

        kwargs = {'batch_size': train_kwargs['batchsize']} if 'batchsize' in train_kwargs else {}
        print('cross-validation loss:', model.evaluate(inputs_xval, outputs_xval, **kwargs))

        print('computing r2 metric for cross-validation dataset...')
        r2_xval = true_corr(model, inputs_xval, outputs_xval, train_kwargs)
        print('r^2 for xval:', r2_xval)

        inputs, outputs = prep_for_net(X, y, x_ids_labels, y_ids_labels)
        print('continuing to train on all data')
        kwargs = {'batchsize': train_kwargs['batchsize']} if 'batchsize' in train_kwargs else {}
        model = retrain_net(model, inputs, outputs, train_loss_at_best_xval, epoch_best_xval, **kwargs)

        # @TODO: add layer for de-standardization

        model.save(net_filename, overwrite=True)

    if do_predict:
        test_inputs, _ = prep_for_net(X_test, y=None, x_ids_labels=x_ids_labels, y_ids_labels=None)
        kwargs = {'batch_size': 1}
        # kwargs = {'batch_size': train_kwargs['batchsize']} if 'batchsize' in train_kwargs else {}
        y_pred_test = model.predict(test_inputs, verbose=1, **kwargs)
        y_pred_test *= y_std[0]
        y_pred_test += y_mean[0]
        pass

    # @TODO: sklearn wrapper


if __name__ == '__main__':
    main()
