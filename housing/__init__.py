#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
this package trains a predictor for house prices given a few features like post-code, time of sale etc.
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"
