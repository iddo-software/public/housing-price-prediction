#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
preprocess data: convert categorical labels to one-hot format, apply standardization on continuous variables
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

from six.moves import xrange

import numpy


class Vals2Labels(object):
    def __init__(self, vals):
        self.back_dict = numpy.unique(vals)
        self.fwd_dict = dict((val, idx) for idx, val in enumerate(self.back_dict))

        if len(self.back_dict) <= numpy.iinfo(numpy.uint8).max:
            self.label_dtype = numpy.uint8
        elif len(self.back_dict) <= numpy.iinfo(numpy.uint16).max:
            self.label_dtype = numpy.uint16
        elif len(self.back_dict) <= numpy.iinfo(numpy.uint32).max:
            self.label_dtype = numpy.uint32
        elif len(self.back_dict) <= numpy.iinfo(numpy.uint64).max:
            self.label_dtype = numpy.uint64
        elif len(self.back_dict) <= numpy.iinfo(numpy.uint128).max:
            self.label_dtype = numpy.uint128
        else:
            raise TypeError('too many classes for standard numpy integers; find something else')

    def fwd(self, vals):
        # return numpy.asarray([self.fwd_dict[val] for val in vals], dtype=self.label_dtype)
        # avoid making a huge list
        labels = numpy.empty(len(vals), self.label_dtype)
        for val_idx, val in enumerate(vals):
            labels[val_idx] = self.fwd_dict[val]
        return labels

    def back(self, labels):
        # return numpy.asarray([self.back_dict[label] for label in labels])
        return self.back_dict[labels]


class Labels2OneHot(object):
    def __init__(self, n_labels):
        self.n_labels = n_labels

    def fwd(self, labels):
        onehots = numpy.empty((len(labels), self.n_labels), bool)
        onehots[xrange(len(labels)), labels] = True
        return onehots

    def back(self, onehots):
        return onehots.argmax(axis=1)


class Vals2OneHot(object):
    def __init__(self, vals):
        self.vals2labels = Vals2Labels(vals)
        self.labels2onehot = Labels2OneHot(len(self.vals2labels.fwd_dict))

    def fwd(self, vals):
        labels = self.vals2labels.fwd(vals)
        onehot = self.labels2onehot.fwd(labels)
        return onehot

    def back(self, onehot):
        labels = self.labels2onehot.back(onehot)
        vals = self.vals2labels.back(labels)
        return vals


class StructArr2Categorical(object):
    def __init__(self, struct_arr, onehot_field_names, label_field_names):
        assert not (set(onehot_field_names) & set(label_field_names))
        self.converters = {field_name: Vals2OneHot(struct_arr[field_name]) for field_name in onehot_field_names}
        for field_name in label_field_names:
            self.converters[field_name] = Vals2Labels(struct_arr[field_name])
        self.struct_dtype = struct_arr.dtype
        self.onehot_field_names = {}
        for field_name in onehot_field_names:
            n_classes = self.converters[field_name].labels2onehot.n_labels
            self.onehot_field_names[field_name] = [field_name + '_' + str(class_id) for class_id in xrange(n_classes)]

        self.label_field_names = {}
        for field_name in label_field_names:
            self.label_field_names[field_name] = field_name + '_label'

    def fwd(self, struct_arr):
        field_dtypes = []
        for field_name in struct_arr.dtype.names:
            if field_name in self.onehot_field_names:
                field_dtypes.extend([(onehot_field_name, bool) for onehot_field_name in self.onehot_field_names[field_name]])
            elif field_name in self.label_field_names:
                field_dtypes.append((self.label_field_names[field_name], self.converters[field_name].label_dtype))
            else:
                field_dtypes.append((field_name, struct_arr.dtype.fields[field_name][0]))

        out = numpy.empty(len(struct_arr), field_dtypes)

        for field_name in struct_arr.dtype.names:
            if field_name in self.converters:
                categorical = self.converters[field_name].fwd(struct_arr[field_name])
                if field_name in self.onehot_field_names:
                    for class_id, onehot_field_name in enumerate(self.onehot_field_names[field_name]):
                        out[onehot_field_name] = categorical[:, class_id]
                else:
                    out[self.label_field_names[field_name]] = categorical
            else:
                out[field_name] = struct_arr[field_name]

        return out

    def back(self, one_hot_struct_arr):
        out = numpy.empty(len(one_hot_struct_arr), self.struct_dtype)
        for field_name in self.struct_dtype.names:
            if field_name in self.onehot_field_names:
                onehot = one_hot_struct_arr[self.onehot_field_names[field_name]]
                out[field_name] = self.converters[field_name].back(onehot)
            else:
                out[field_name] = one_hot_struct_arr[field_name]

        return out


class Preproc(object):
    def __init__(self, means, stds):
        self.means = means
        self.stds = stds

    def fit(self, data):
        NotImplementedError('we should not get here in our toy example')

    def predict(self, data):
        return (data - self.means) / self.stds

    def back(self, preprocd):
        return preprocd * self.stds + self.means


class StructPreproc(object):
    def __init__(self, drop, continuous, invalid_vals, target_names):
        self.drop = drop
        self.continuous = continuous
        self.invalid_vals = invalid_vals
        self.target_names = target_names
        self.means = {}
        self.stds = {}

        self.x_preproc = None
        self.y_preproc = None

    def fit(self, data):
        cont_cols = []
        all_valid_mask = numpy.ones(len(data), bool)

        x_means = numpy.empty(len(data.dtype.names) - len(self.target_names))
        x_stds = numpy.empty_like(x_means)

        y_means = numpy.empty(len(self.target_names))
        y_stds = numpy.empty_like(y_means)

        x_col_idx = 0
        y_col_idx = 0

        for field_name in data.dtype.names:
            if field_name in self.continuous:
                col = data[field_name]
                valid_mask = col != self.invalid_vals[field_name]
                valids = col[valid_mask]

                mean = valids.mean()
                std = valids.std()

                self.means[field_name] = mean
                self.stds[field_name] = std

                if field_name not in self.target_names:
                    cont_cols.append(col)
                    all_valid_mask &= valid_mask
            else:
                mean = 0
                std = 1

            if field_name in self.target_names:
                y_means[y_col_idx] = mean
                y_stds[y_col_idx] = std
                y_col_idx += 1
            else:
                x_means[x_col_idx] = mean
                x_stds[x_col_idx] = std
                x_col_idx += 1

        self.x_preproc = Preproc(x_means, x_stds)
        self.y_preproc = Preproc(y_means, y_stds)

        if len(cont_cols) < 2:
            return

        # we could do ZCA whitening here to get rid of linear correlations between design variables, but we only got a
        # single continuous design variable (namely, the date). If, e.g., the price were a design variable, too, ZCA
        # whitening would account for the linear increase of price over time, thus allowing us to consider the two
        # measures independently, as their deviation from the linear trend (of houses, probably, getting more expensive
        # over time)

        # cont = numpy.vstack([cont_col[all_valid_mask] for cont_col in cont_cols]).
        # standardize cont -> compute correlation matrix for cont -> svd it and save vectors / variances
        # at predict time: after standardization, rotate data in PCA space, divide by sqrt(variance), rotate it back in
        # original space
        raise NotImplementedError()

    def predict(self, data):
        out_dtypes = []

        for field_name in data.dtype.names:
            if field_name in self.drop:
                continue
            if field_name in self.continuous:
                out_dtypes.append((field_name, numpy.float32))
            else:
                out_dtypes.append((field_name, data.dtype.fields[field_name][0]))

        preprocd = numpy.empty(len(data), out_dtypes)

        for field_name in preprocd.dtype.names:
            if field_name in self.continuous:
                col = data[field_name]
                preprocd[field_name] = numpy.where(col != self.invalid_vals[field_name],
                                                   (col - self.means[field_name]) / self.stds[field_name],
                                                   self.invalid_vals[field_name])
            else:
                preprocd[field_name] = data[field_name]

        return preprocd

    def back(self, preprocd):
        data = numpy.empty_like(preprocd)
        for field_name in preprocd.dtype.names:
            if field_name in self.continuous:
                data[field_name] = preprocd[field_name] * self.stds[field_name] + self.means[field_name]
        raise NotImplementedError()


def split(data, y_names, invalid_vals, label_names):
    """
    assumes that, if a record has an invalid value on any of the y, then it is part of the test set
    :param data:
    :param y_names:
    :param invalid_vals:
    :return:
    """
    test_mask = numpy.zeros(len(data), bool)
    for y_name in y_names:
        test_mask |= data[y_name] == invalid_vals[y_name]

    n_test = numpy.count_nonzero(test_mask)
    n_train = len(data) - n_test

    y = numpy.empty((n_train, len(y_names)), numpy.float32)

    y_ids_labels = []

    for col_id, y_name in enumerate(y_names):
        y[:, col_id] = data[y_name][~test_mask]
        if y_name in label_names:
            y_ids_labels.append(col_id)

    X = numpy.empty((n_train, len(data.dtype.names) - len(y_names)), numpy.float32)
    X_test = numpy.empty((n_test, X.shape[1]), numpy.float32)

    x_ids_labels = []

    for col_id, x_name in enumerate([name for name in data.dtype.names if name not in y_names]):
        X[:, col_id] = data[x_name][~test_mask]
        X_test[:, col_id] = data[x_name][test_mask]
        if x_name in label_names:
            x_ids_labels.append(col_id)

    return X, y, X_test, x_ids_labels, y_ids_labels
