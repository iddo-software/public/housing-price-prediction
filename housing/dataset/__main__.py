#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
test script for loading and preprocessing data
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"


def main():
    from housing.dataset.load import load
    # force_categorical to rebuild pickles after refactor
    load(data_dir='data/', force_categorical=True)


if __name__ == '__main__':
    main()
