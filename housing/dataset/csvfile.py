#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
read data from csv and do some basic preproc (fix csv, data format, break down post code)
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

import numpy


def fix_csv_commas(in_filename, out_filename, lineterminator='\n'):
    """
    the provided csv file contains commas within the values of columns, which is treated as a delimiter by
    numpy.genfromtxt (it does not support quote characters), so we onehotess it like this first. An alternative would
    be to use pandas, which might contain better csv readers than numpy. Given the fact that the input data is not
    that big, and that this function does not keep all the data in memory, the only disadvantage is that it's going to
    slow the process down a bit in the first stage, which will be disabled in subsequent runs, so we can do this
    :param in_filename: csv file, formatted as part2_data.csv
    :param out_filename: the name of the fixed csv file, which can be further passed to numpy.genfromtxt
    :param lineterminator: although csv.reader automatically detects the line ending in the file, it does not update the
                           corresponding field in its dialect object, so we must specify it explicitly; defaults to '\n'
                           (as observed in the part2_data.csv)
    :return: False if out_filename already exists and force is False; True, otherwise
    """
    import csv
    import numpy

    max_chars = None

    with open(in_filename) as infile, open(out_filename, 'w') as outfile:
        # default params are the ones for excel dialect, i.e., the one that the reader defaults to
        # delimiter = ','  # default
        # quotechar = '"'  # default
        doublequote = False
        # lineterminator = '\n'  # passed as parameter
        # escapechar = None  # default
        strict = True  # raise Error when bad csv
        # quoting = csv.QUOTE_MINIMAL  # default

        reader = csv.reader(infile, doublequote=doublequote, strict=strict, lineterminator=lineterminator)
        writer = csv.writer(outfile, dialect=reader.dialect, quoting=csv.QUOTE_NONE)

        for row_id, row in enumerate(reader):
            if max_chars is None:
                max_chars = numpy.zeros(len(row), numpy.int32)

            for cell_id, cell in enumerate(row):
                cell = cell.replace(', ', ' ').replace(',', ' ')
                row[cell_id] = cell
                if row_id != 0:
                    # skip the header row
                    max_chars[cell_id] = max(max_chars[cell_id], len(cell))

            writer.writerow(row)
    return max_chars


def breakdown_postcode(postcode):
    # postcode:
    #   - outward code
    #       - postcode area
    #       - postcode district
    #   - inward code
    #       - postcode sector
    #       - postcode unit
    if postcode == '':
        return '', '', -1, ''
    outward, inward = postcode.split()
    if outward[1].isdigit():
        area = outward[0]
        district = outward[1:]
    else:
        area = outward[:2]
        district = outward[2:]

    sector = inward[0]
    unit = inward[1:]
    return area, district, numpy.int8(sector), unit


def read_csv(data_filename_fixed, max_chars):
    import datetime
    from six.moves import xrange

    field_dtypes = [numpy.int32,          # ID
                    numpy.int32,          # Price
                    numpy.int32,          # Date (after conversion)
                    str,          # Postcode
                    numpy.uint8,  # Property_Type
                    bool,         # Old_New
                    numpy.uint8,  # Duration (can't have it as bool, as undocumented values occur)
                    str,          # Street
                    str,          # Locality
                    str,          # Town
                    str,          # District
                    str,          # County
                    bool          # PPD_Category_Type
                    ]

    for field_id, field_dtype in enumerate(field_dtypes):
        if field_dtype == str:
            field_dtypes[field_id] = 'U' + str(int(max_chars[field_id]))

    property_type_labels = {
        'D': 0,
        'S': 1,
        'T': 2,
        'F': 3,
        'O': 4
    }

    def bytes2str(str_or_bytes):
        from six import string_types
        if isinstance(str_or_bytes, string_types):
            str_sure = str_or_bytes
        else:
            assert isinstance(str_or_bytes, bytes)
            str_sure = str_or_bytes.decode('latin1')

        return str_sure

    def property_type_converter(property_type_str):
        property_type_str = bytes2str(property_type_str)
        try:
            return property_type_labels[property_type_str]
        except KeyError:
            raise ValueError('invalid value for property type')

    def old_new_converter(old_new_str):
        old_new_str = bytes2str(old_new_str)
        if old_new_str == 'Y':
            return True
        elif old_new_str == 'N':
            return False
        else:
            raise ValueError('invalid value for old / new')

    def duration_converter(duration_str):
        duration_str = bytes2str(duration_str)
        if duration_str == 'F':
            return 0
        elif duration_str == 'L':
            return 1
        else:
            return 2  # 'U' values also encountered; not documented, so we assume they are unknown

    def ppd_category_type_converter(ppd_category_type_str):
        ppd_category_type_str = bytes2str(ppd_category_type_str)
        if ppd_category_type_str == 'A':
            return True
        elif ppd_category_type_str == 'B':
            return False
        else:
            raise ValueError('invalid value for ppd category type')

    def date_converter(date_str):
        date_str = bytes2str(date_str)
        return datetime.datetime.strptime(date_str, '%Y-%m-%d').date().toordinal()

    converters = {
        'date': date_converter,
        'property_type': property_type_converter,
        'old_new': old_new_converter,
        'duration': duration_converter,
        'ppd_category_type': ppd_category_type_converter
    }

    # max_rows = 10  # for debugging
    max_rows = None

    data = numpy.genfromtxt(data_filename_fixed, field_dtypes, delimiter=',', names=True, case_sensitive='lower',
                            max_rows=max_rows, converters=converters, loose=False, encoding='latin1')

    assert -1 not in data['id']
    # assert -1 not in data['price']  # prices for test data are unknown
    # assert -1 not in data['date']
    # assert -1 not in data['property_type']

    drop_fields = set()

    changed_field_dtypes = {}

    if numpy.all(data['id'] == range(len(data))):
        drop_fields.add('id')

    # if numpy.iinfo(numpy.int32).max >= data['price'].max():
    #     changed_field_dtypes['price'] = numpy.int32

    data['date'] -= data['date'].min()  # make earliest date 0

    if numpy.iinfo(numpy.uint16).max >= data['date'].max():
        changed_field_dtypes['date'] = numpy.uint16

    # filter out columns with useless data (e.g., constant features, features the values of which are encountered
    # only once in the whole dataset)
    for field_name in data.dtype.names:
        n_unique = len(numpy.unique(data[field_name]))
        if n_unique == 1 or n_unique == len(data):
            drop_fields.add(field_name)

    # break up postcode
    postcode_components = numpy.empty(len(data), dtype=[('post_area', 'S2'), ('post_district', 'S2'),
                                                        ('post_sector', numpy.int8), ('post_unit', 'S2')])
    for row_id in xrange(len(data)):
        postcode_components[row_id] = breakdown_postcode(data['postcode'][row_id])

    drop_fields.add('postcode')

    # filter out columns which don't add any additional information
    loc_fields = ['county', 'district', 'town', 'locality', 'street']
    all_combined = data[loc_fields[0]]
    for loc_field in loc_fields[1:]:
        all_combined = numpy.core.defchararray.add(all_combined, data[loc_field])

    n_all_unique = len(numpy.unique(all_combined))

    loc_fields = loc_fields[::-1]

    combined = None
    while len(loc_fields) > 1:
        if combined is None:
            combined = data[loc_fields.pop()]
        else:
            combined = numpy.core.defchararray.add(combined, data[loc_fields.pop()])

        if len(numpy.unique(combined)) == n_all_unique:
            while loc_fields:
                drop_fields.add(loc_fields.pop())

    just_street = data['street'].copy()
    for row_id in xrange(len(just_street)):
        words = just_street[row_id].split(' ')
        try:
            int(words[0])
        except ValueError:
            start = 0
        else:
            start = 1

        if words[-2] == 'FLAT':
            # this should eliminate 'flat A' and 'flat 23'
            end = -2
        else:
            try:
                int(words[-1])
            except ValueError:
                end = len(words)
            else:
                end = -1

        just_street[row_id] = ' '.join(words[start:end])

    if len(numpy.unique(numpy.core.defchararray.add(combined, data['street']))) == n_all_unique:
        data['street'] = just_street

    # @NOTE: it turns out that none of the address-related columns is redundant; for further redundant feature
    #        detection, one could, perhaps, try to remove some of the components of the post code, if perfect overlap
    #        is observed between the component and the other geographical data (if, i.e., all the properties within
    #        a single postcode district lie in the same administrative district, and all the properties within that
    #        administrative district lie in the same postcode district, than the information is redundant. If this
    #        holds true for all the administrative / postcode districts, than one of the columns can be safely removed)

    new_field_dtypes = []

    for field_name in data.dtype.names:
        if field_name in drop_fields:
            continue

        if field_name in changed_field_dtypes:
            field_dtype = changed_field_dtypes[field_name]
        else:
            field_dtype = data.dtype.fields[field_name][0]

        new_field_dtypes.append((field_name, field_dtype))

    field_names_kept = [new_field_dtype[0] for new_field_dtype in new_field_dtypes]

    # add the columns of postcode components
    additional_field_dtypes = []
    additional_field_names = []
    for field_name in postcode_components.dtype.names:
        additional_field_names.append(field_name)
        additional_field_dtypes.append((field_name, postcode_components.dtype.fields[field_name][0]))

    new_data = numpy.empty(len(data), new_field_dtypes + additional_field_dtypes)

    for field_name_kept in field_names_kept:
        new_data[field_name_kept] = data[field_name_kept]

    for additional_field_name in additional_field_names:
        new_data[additional_field_name] = postcode_components[additional_field_name]

    return new_data
