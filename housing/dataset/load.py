#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
read data from csv and apply preprocessing; cache results (intermediate and final) so we don't have to run everything
every time we experiment with training a new model
"""

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"

import numpy


def load(data_dir, force_fix_csv=False, force_read_csv=False, force_categorical=False, force_preproc=False,
         force_split=False):
    """
    kind of a script (some params are hardcoded, as we don't use a proper persistent memoization mechanism, i.e., which
    triggers recomputation when code changes; basically, some sort of versioning is required for that)
    :param data_dir:
    :return:
    """
    import os
    try:
        import cPickle as pickle
    except ImportError:
        import pickle
    from housing.dataset.csvfile import fix_csv_commas, read_csv
    from housing.dataset.preproc import StructArr2Categorical, StructPreproc, split

    # param for cache location (we cache all intermediate results because some of the steps take a long time)

    # params for csv preproc
    csv_filename = 'part2_data.csv'
    csv_filename_fixed = 'part2_data_fixed.csv'
    max_char_filename = 'max_chars.npy'

    # params for read csv
    data_filename = 'data.npy'

    # params for onehot data
    categorical_filename = 'onehot.npy'
    categorical_convertor_filename = 'onehot_convertor.pkl'
    # we don't include the street and locality identifiers, as there are too many (63K, 9K)
    onehot_fields = ['post_area', 'post_district', 'post_sector', 'post_unit', 'town', 'district', 'county']
    label_fields = ['street', 'locality']

    # params for data preproc
    drop = {'id'}  # drop 'ID' column
    # standardize continuous columns
    continuous = {'price', 'date'}

    invalid_vals = {'price': -1,
                    'date': -1}

    preprocd_filename = 'preprocd.npy'
    preproc_filename = 'preproc.pkl'

    # params for split
    y_names = ['price']
    split_filename = 'split.npy'

    # end of user param area

    csv_filename = os.path.join(data_dir, csv_filename)
    csv_filename_fixed = os.path.join(data_dir, csv_filename_fixed)
    max_char_filename = os.path.join(data_dir, max_char_filename)
    data_filename = os.path.join(data_dir, data_filename)
    categorical_filename = os.path.join(data_dir, categorical_filename)
    categorical_convertor_filename = os.path.join(data_dir, categorical_convertor_filename)
    preprocd_filename = os.path.join(data_dir, preprocd_filename)
    preproc_filename = os.path.join(data_dir, preproc_filename)
    split_filename = os.path.join(data_dir, split_filename)

    fix_csv_cached = os.path.isfile(csv_filename_fixed) and os.path.isfile(csv_filename_fixed) and not force_fix_csv
    read_csv_cached = os.path.isfile(data_filename) and not force_read_csv and fix_csv_cached
    categorical_cached = (os.path.isfile(categorical_filename) and os.path.isfile(categorical_convertor_filename)
                          and not force_categorical and read_csv_cached)
    preproc_cached = (os.path.isfile(preproc_filename) and os.path.isfile(preprocd_filename) and not force_preproc
                      and categorical_cached)
    split_cached = os.path.isfile(split_filename) and not force_split and preproc_cached

    if not read_csv_cached:
        # fix csv
        if fix_csv_cached:
            max_chars = numpy.load(max_char_filename)
        else:
            max_chars = fix_csv_commas(csv_filename, csv_filename_fixed)
            numpy.save(max_char_filename, max_chars)

    if not categorical_cached:
        # read fixed csv
        if read_csv_cached:
            data = numpy.load(data_filename)
        else:
            data = read_csv(csv_filename_fixed, max_chars)
            numpy.save(data_filename, data)

    # we need converter for the output, not just for the next step in the pipeline (preproc)
    if categorical_cached:
        with open(categorical_convertor_filename, 'rb') as onehot_convertor_file:
            converter = pickle.load(onehot_convertor_file)
    else:
        converter = StructArr2Categorical(data, onehot_fields, label_fields)
        with open(categorical_convertor_filename, 'wb') as onehot_convertor_file:
            pickle.dump(converter, onehot_convertor_file)

    if not preproc_cached:
        # turn into onehot data where possible, or, at least into numeric labels instead of strings
        if categorical_cached:
            data = numpy.load(categorical_filename)
        else:
            data = converter.fwd(data)
            numpy.save(categorical_filename, data)

    # we need preproc for the output, not just for the next step in the pipeline (split)
    if preproc_cached:
        with open(preproc_filename, 'rb') as preproc_file:
            preproc = pickle.load(preproc_file)
    else:
        preproc = StructPreproc(drop, continuous, invalid_vals, y_names)
        # @TODO: for a real life task, for a better estimate of the performance of the model, one would also have
        #        to fit the preprocessor ONLY on the training data (i.e., after excluding the cross-validation set),
        #        in order to measure how well it generalizes to data it has never seen before
        preproc.fit(data)
        with open(preproc_filename, 'wb') as preproc_filename:
            pickle.dump(preproc, preproc_filename)

    if not split_cached:
        if preproc_cached:
            data = numpy.load(preprocd_filename)
        else:
            data = preproc.predict(data)
            numpy.save(preprocd_filename, data)

    if split_cached:
        with open(split_filename, 'rb') as split_file:
            X = numpy.load(split_file)
            y = numpy.load(split_file)
            X_test = numpy.load(split_file)
            x_ids_labels = numpy.load(split_file)
            y_ids_labels = numpy.load(split_file)
    else:
        X, y, X_test, x_ids_labels, y_ids_labels = split(data, y_names, invalid_vals,
                                                         [converter.label_field_names[label_field] for label_field in label_fields])
        with open(split_filename, 'wb') as split_file:
            numpy.save(split_file, X)
            numpy.save(split_file, y)
            numpy.save(split_file, X_test)
            numpy.save(split_file, x_ids_labels)
            numpy.save(split_file, y_ids_labels)

    # sanity check:
    assert numpy.isfinite(X).all()
    assert numpy.isfinite(y).all()
    assert numpy.isfinite(X_test).all()

    y_means = [preproc.means[y_name] for y_name in y_names]
    y_stds = [preproc.stds[y_name] for y_name in y_names]

    return X, y, X_test, preproc.x_preproc, preproc.y_preproc, x_ids_labels, y_ids_labels, y_means, y_stds


def subset(X, y, size=.1, rng=None):
    """

    :param X:
    :param y:
    :param size: 0 <= size <= 1; ratio between subset size and all data size
    :param rng: if None, no shuffle, if int, seed, else numpy.random.RandomState impl
    :return:
    """
    data_size = len(y)
    assert len(X) == data_size

    size = size or 1.

    subset_size = int(round(data_size * size))

    try:
        rng = int(rng)
    except TypeError:
        # already a rng instance, or None
        pass
    else:
        rng = numpy.random.RandomState(rng)

    if rng:
        perm = rng.permutation(data_size)
        X = X[perm[:subset_size]]
        y = y[perm[:subset_size]]
    else:
        X = X[:subset_size]
        y = y[:subset_size]

    return X, y
