#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
main script: trains sklearn regression baseline, loads neural net from disk and evaluates them both
"""

from __future__ import print_function

__author__ = "Iddo Software <dev@iddo.ro>"
__copyright__ = "Copyright © 2017, Iddo Software. All Rights Reserved."
__license__ = "Proprietary"
__version__ = "0.1"
__maintainer__ = "Iddo Software <support@iddo.ro>"
__email__ = "dev@iddo.ro"
__status__ = "Development"


def main():
    from housing.predict.util import Evaluator, baseline
    from housing.dataset.load import load
    from keras.models import load_model
    from housing.predict.nnet.models import NegCorr

    data_dir = 'data/'
    net_filename = 'final_net.pkl'

    X, y, X_test, X_preproc, y_preproc, x_ids_labels, y_ids_labels = load(data_dir)

    evaluate = Evaluator(X, y.flatten())

    baseline_model = baseline(X, y.flatten())
    baseline_score = evaluate(baseline_model)

    net = load_model(net_filename, custom_objects={'neg_corr': NegCorr()})

    nnet_score = evaluate(net)

    print('baseline score:', baseline_score)
    print('nnet score:', nnet_score)


if __name__ == '__main__':
    main()
