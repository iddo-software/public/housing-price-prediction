from __future__ import print_function

import inspect


def f(a, b=2, c=3):
    print(inspect.formatargvalues(*inspect.getargvalues(inspect.currentframe())))


def main():
    f(1, 4, c=5)


if __name__ == '__main__':
    main()